unit SubmeterUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, sqlite3conn, db, FileUtil, Forms, Controls,
  Graphics, Dialogs, DBGrids, StdCtrls, Grids, ExtCtrls, Buttons,
  Menus, windows, comobj, variants, Printers;

type

  { TForm1 }

  TForm1 = class(TForm)
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    EditHeight: TLabeledEdit;
    EditDopusk: TLabeledEdit;
    EditWidth: TLabeledEdit;
    EditBorder: TLabeledEdit;
    EditName: TLabeledEdit;
    MenuItemDelete: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    PopupMenu1: TPopupMenu;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButtonWord: TRadioButton;
    RadioButtonExportExcel: TRadioButton;
    RadioButtonExportText: TRadioButton;
    RadioGroup1: TRadioGroup;
    ButtonConnect: TSpeedButton;
    ButtonCalculate: TSpeedButton;
    ButtonExport: TSpeedButton;
    ButtonExit: TSpeedButton;
    RadioGroup2: TRadioGroup;
    ButtonAdd: TSpeedButton;
    SaveDialog1: TSaveDialog;
    SQLConnector1: TSQLConnector;
    SQLQuery1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    Grid: TStringGrid;
    procedure ButtonCalculateClick(Sender: TObject);
    procedure ButtonConnectClick(Sender: TObject);
    procedure ButtonExportClick(Sender: TObject);
    procedure ButtonExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ButtonAddClick(Sender: TObject);
    procedure GetDataFromDB;
    procedure MenuItemDeleteClick(Sender: TObject);
    procedure GridClick(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure Calculate;
    procedure RadioGroup1Click(Sender: TObject);
  private
    { private declarations }
    myWidth  : integer;
    myHeight : integer;
    mydopusk : integer;
    myKray   : integer;
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  Word:variant;
  sortDirection : Boolean;

implementation

{$R *.lfm}

{ TForm1 }

function CreateWord:boolean;
begin
  CreateWord := true;
   try
    Word := CreateOleObject('Word.Application');
   except
    CreateWord := False;
   end;
end;

function VisibleWord(Visible:boolean):boolean;
begin
  VisibleWord := True;
   try
    Word.Visible := Visible;
   except
    VisibleWord := False;
   end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  myWidth:=1000;
  myHeight:=100;;
  SetWindowLongPtr(
    EditWidth.Handle,
    GWL_STYLE,
    GetWindowLongPtr(EditWidth.Handle, GWL_STYLE) or ES_NUMBER
    );
  SetWindowLongPtr(
    EditHeight.Handle,
    GWL_STYLE,
    GetWindowLongPtr(EditHeight.Handle, GWL_STYLE) or ES_NUMBER
    );
  SetWindowLongPtr(
    EditDopusk.Handle,
    GWL_STYLE,
    GetWindowLongPtr(EditDopusk.Handle, GWL_STYLE) or ES_NUMBER
    );
  SetWindowLongPtr(
    EditBorder.Handle,
    GWL_STYLE,
    GetWindowLongPtr(EditBorder.Handle, GWL_STYLE) or ES_NUMBER
    );
end;

procedure TForm1.ButtonAddClick(Sender: TObject);
var
  mwidth : integer;
  mheight : integer;
  mname : string;
  sqlstring : string;
  i : integer;
begin
  if EditWidth.Text = '' then begin
    EditWidth.Text:='0';
    mwidth := 0;
  end else begin
    mwidth:=StrToInt(EditWidth.Text);
  end;
  if EditHeight.Text = '' then begin
    EditHeight.Text:='0';
    mheight := 0;
  end else begin
    mheight:=StrToInt(EditHeight.Text);
  end;

  mname := EditName.Text;

  sqlstring:='INSERT INTO "main" ("width","height","name") VALUES ('
    +IntToStr(mwidth)
    +','
    +IntToStr(mheight)
    +',"'
    +mname
    +'");';

  SQLTransaction1.Commit;
  SQLTransaction1.StartTransaction;
  SQLConnector1.ExecuteDirect(sqlstring);
  SQLTransaction1.Commit;

  Grid.Clear;

  GetDataFromDB;

end;

procedure TForm1.GetDataFromDB;
var
  i : integer;
begin
  SQLQuery1.Close;
  SQLQuery1.SQL.Text:='SELECT * FROM main ORDER BY width;';
  SQLQuery1.Open;
  i := 1;
  Grid.RowCount:=1;
  while (not SQLQuery1.EOF) do begin
    Grid.RowCount:=Grid.RowCount + 1;
    Grid.Cells[1, Grid.RowCount - 1] := IntToStr(i);
    Grid.Cells[2, Grid.RowCount - 1] := SQLQuery1.FieldByName('width').AsString;
    Grid.Cells[3, Grid.RowCount - 1] := SQLQuery1.FieldByName('height').AsString;
    Grid.Cells[4, Grid.RowCount - 1] := SQLQuery1.FieldByName('name').AsString;
    Grid.Cells[5, Grid.RowCount - 1] := '-';
    Grid.Cells[6, Grid.RowCount - 1] := '-';
    i := i + 1;
    SQLQuery1.Next;
  end;

end;

procedure TForm1.MenuItemDeleteClick(Sender: TObject);
var
  w : integer;
  h : integer;
  row : integer;
  sqlstring : string;
begin
  row := Grid.Row;
  w := StrToInt(Grid.Cells[2, row]);
  h := StrToInt(Grid.Cells[3, row]);
  sqlstring:='DELETE FROM "main" WHERE "width"='
      +IntToStr(w)
      +' and "height"='
      +IntToStr(h)
      +';';

    SQLTransaction1.Commit;
    SQLTransaction1.StartTransaction;
    SQLConnector1.ExecuteDirect(sqlstring);
    SQLTransaction1.Commit;

    Grid.Clear;

    GetDataFromDB;
end;

procedure TForm1.GridClick(Sender: TObject);
var
  row : integer;
begin
  row := Grid.Row;
  EditWidth.Text := Grid.Cells[2, row];
  EditHeight.Text := Grid.Cells[3, row];
  EditName.Text := Grid.Cells[4, row];
end;

procedure TForm1.GridDblClick(Sender: TObject);
var
  n, i, j : integer;
  s : string;
begin
  n := Grid.RowCount;

  if (sortDirection) then begin
    for i := 1 to n - 2 do begin
      for j := i + 1 to n - 1 do begin
        if StrToInt(Grid.Cells[2, i]) > StrToInt(Grid.Cells[2,j]) then begin
          Grid.Cols[2].Exchange(j, i);
          Grid.Cols[3].Exchange(j, i);
          Grid.Cols[4].Exchange(j, i);
          Grid.Cols[5].Exchange(j, i);
          Grid.Cols[6].Exchange(j, i);
        end;
      end;
    end;
    sortDirection:=False;
  end else begin
    for i := 1 to n - 2 do begin
      for j := i + 1 to n - 1 do begin
        if StrToInt(Grid.Cells[2, i]) < StrToInt(Grid.Cells[2,j]) then begin
          Grid.Cols[2].Exchange(j, i);
          Grid.Cols[3].Exchange(j, i);
          Grid.Cols[4].Exchange(j, i);
          Grid.Cols[5].Exchange(j, i);
          Grid.Cols[6].Exchange(j, i);
        end;
      end;
    end;
    sortDirection:=True;
  end;

end;

procedure TForm1.Calculate;
var
  i : integer;
  j : integer;
  w : integer;
  h : integer;
  o : integer;
  p : integer;
  l : integer;
  k : integer;
begin
  myDopusk:=StrToInt(EditDopusk.Text);
  myKray:=StrToInt(EditBorder.Text);
  l := myWidth - 2*myKray - myDopusk;
  for i := 1 to Grid.RowCount - 1 do begin
    w := StrToInt(Grid.Cells[2, i]) + myDopusk;
    h := StrToint(Grid.Cells[3, i]);
    p := l div w;
    o := l - p * w + mydopusk;
    Grid.Cells[5, i] := IntToStr(p);
    Grid.Cells[6, i] := IntToStr(o);
  end;
end;

procedure TForm1.RadioGroup1Click(Sender: TObject);
begin

end;

procedure TForm1.ButtonConnectClick(Sender: TObject);
begin
  Grid.Clear;
  GetDataFromDB;
  sortDirection:=False;
  ButtonAdd.Enabled:=True;
  ButtonCalculate.Enabled:=True;
  ButtonExport.Enabled:=True;
  ButtonExit.Enabled:=True;
end;

procedure TForm1.ButtonExportClick(Sender: TObject);
var
  i, j : integer;
  filename : string;
  f : text;
  N : integer;
  CSV : TStrings;
  s : string;
  ExcelApp,
  ExcelSheet,
  ExcelCol,
  ExcelRow: Variant;
  Size: Byte;
begin
  if RadioButtonExportExcel.Checked then begin
      try
        ExcelApp:=CreateOleObject('Excel.Application');
        ExcelApp.Visible:=true;
        ExcelApp.Workbooks.Add(-4167);
        ExcelApp.Workbooks[1].WorkSheets[1].Name:='Submeter report';
        ExcelCol:=ExcelApp.Workbooks[1].WorkSheets['Submeter report'].Columns;
        Size:=Grid.DefaultRowHeight;
        N:=Grid.ColCount - 1;

        for j := 0 to N do
          ExcelCol.Columns[j + 1].ColumnWidth:=Size;
          ExcelRow:=ExcelApp.Workbooks[1].WorkSheets['Submeter report'].Rows;
          ExcelRow.Rows[1].Font.Bold:=true;
          ExcelSheet:=ExcelApp.Workbooks[1].WorkSheets['Submeter report'];
          ExcelSheet.Cells.Item[1, 3].Value := WideString(utf8Decode('Номер'));
          ExcelSheet.Cells.Item[1, 4].Value := WideString(utf8Decode('Ширина'));
          ExcelSheet.Cells.Item[1, 5].Value := WideString(utf8Decode('Высота'));
          ExcelSheet.Cells.Item[1, 6].Value := WideString(utf8Decode('Метка'));
          ExcelSheet.Cells.Item[1, 7].Value := WideString(utf8Decode('Поместится'));
          ExcelSheet.Cells.Item[1, 8].Value := WideString(utf8Decode('Остаток'));
          for i := 0 to Grid.RowCount - 1 do
            for j := 0 to Grid.ColCount - 1 do
              ExcelSheet.Cells.Item[i+2, j+2].Value := WideString(utf8Decode(Grid.Cells[j, i]));
      finally
      end;
  end;
  if RadioButtonExportText.Checked then begin
    if SaveDialog1.Execute then begin
      filename := SaveDialog1.FileName;
    end;
    if filename <> '' then begin
      try
        CSV := TStringList.Create;
        for i := 1 to Grid.RowCount - 1 do begin
          s := '№ '
            + Grid.Cells[1, i]
            + ' Ширина = '
            + Grid.Cells[2, i]
            + '. Высота = '
            + Grid.Cells[3, i]
            + '. Метка: '
            + Grid.Cells[4, i]
            + '. Поместится: '
            + Grid.Cells[5, i]
            + '. Остаток: '
            + Grid.Cells[6, i]
            + '.';
          CSV.Add(s);
        end;
        CSV.SaveToFile(filename)
      finally
        CSV.Free;
      end;
    ShowMessage('Экспорт данных в файл формата CSV выполнен');

    end else begin
      ShowMessage('Пустой путь и имя файла.');
    end;
  end;
  if RadioButtonWord.Checked then begin
    CreateWord;
    word.Documents.add('');
    word.ActiveDocument.Tables.add(Range:=word.ActiveDocument.Range,NumRows:=Grid.RowCount,NumColumns:=Grid.ColCount);
    word.ActiveDocument.Tables.Item(1).Cell(1, 2).Range.InsertBefore(WideString(utf8Decode('№')));
    word.ActiveDocument.Tables.Item(1).Cell(1, 3).Range.InsertBefore(WideString(utf8Decode('Ширина')));
    word.ActiveDocument.Tables.Item(1).Cell(1, 4).Range.InsertBefore(WideString(utf8Decode('Высота')));
    word.ActiveDocument.Tables.Item(1).Cell(1, 5).Range.InsertBefore(WideString(utf8Decode('Метка')));
    word.ActiveDocument.Tables.Item(1).Cell(1, 6).Range.InsertBefore(WideString(utf8Decode('Поместится')));
    word.ActiveDocument.Tables.Item(1).Cell(1, 7).Range.InsertBefore(WideString(utf8Decode('Остаток')));
    for i:=1 to Grid.RowCount do
      for j:=1 to Grid.ColCount do
        word.ActiveDocument.Tables.Item(1).Cell(i, j).Range.InsertBefore(WideString(utf8Decode(Grid.Cells[j-1, i-1])));
    ViSibleWord(True);
  end;
end;

procedure TForm1.ButtonExitClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.ButtonCalculateClick(Sender: TObject);
begin
  if RadioButton1.Checked then begin
    myWidth:=1000;
    myHeight:=100;
  end;
  if RadioButton2.Checked then begin
    myWidth:=1250;
    myHeight:=100;
  end;
  Calculate;
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SQLQuery1.Close;
  SQLTransaction1.Active:=False;
  SQLConnector1.Close();
end;

end.

